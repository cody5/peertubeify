export default {
    peertubeAPI: {
        defaultInstance: 'peertube.social',
        endpoint: 'api/v1',
    },
    invidiousAPI: {
        url: 'https://invidio.us/api/v1',
        videos: 'videos',
        channels: 'channels'
    }
};
