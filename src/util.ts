export function htmlToElement(html: string): Element {
    const template = document.createElement('template');
    template.innerHTML = html.trim();
    return template.content.firstElementChild;
}

export function getPeertubeVideoURL(video, prefs, { isEmbed = false } = {}) {
    const endpoint = isEmbed ? 'embed' : 'watch';

    return `https://${getPeertubeHost(video.account.host, prefs)}/videos/${endpoint}/${video.uuid}`;
}

export function getPeertubeHost(host, prefs) {
    return prefs.openInOriginalInstance ? host : prefs.searchInstance;
}
