/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';
import Preferences from './preferences'
import { RedirectType } from './types';

function id(id: string): Element { return document.getElementById(id); }

const searchInstance = () => id('search-instance') as HTMLInputElement;
const openInOriginalInstance = () => id('open-in-original-instance') as HTMLInputElement;
const resumeUncompletedVideo = () => id('resume-uncompleted-videos') as HTMLInputElement;

const checked = name => (document.querySelector(`input[name="${name}"]:checked`) as HTMLInputElement).value;
const check = name => value => (document.querySelector(`input[name="${name}"][value="${value}"]`) as HTMLInputElement).checked = true

Preferences.getPreferences().then(prefs => {
    searchInstance().value = prefs.searchInstance;
    openInOriginalInstance().checked = prefs.openInOriginalInstance;
    resumeUncompletedVideo().checked = prefs.resumeUncompletedVideo;
    check('redirectPeertube')(prefs.redirectPeertube)
    check('redirectYoutube')(prefs.redirectYoutube)

    function saveOptions(e) {
        e.preventDefault();
        prefs.searchInstance = searchInstance().value;
        prefs.openInOriginalInstance = openInOriginalInstance().checked;
        prefs.resumeUncompletedVideo = resumeUncompletedVideo().checked;
        prefs.redirectPeertube = checked('redirectPeertube') as RedirectType;
        prefs.redirectYoutube = checked('redirectYoutube') as RedirectType;
        prefs.save();
    }

    document.querySelector('form').addEventListener('submit', saveOptions);
})
