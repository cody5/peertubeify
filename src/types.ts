export enum MessageKind {
  SearchByName,
  SearchByID,
}

export enum RedirectType {
    None = 'None',
    Show = 'Show',
    Auto = 'Auto'
}
